const got = require('got');
const { HttpProxyAgent } = require('hpagent');
const config = require('../../config');

const authorization = (auth) => {
  let headers = {};
  const obj = Object.keys(auth);

  if (obj.includes('username') && obj.includes('password')) {
    headers = {
      'user-agent': 'Custom-Request EBCOM',
      Authorization: `Basic ${Buffer.from(`${auth.username}:${auth.password}`).toString('base64')}`,
    };
  } else if (obj.includes('user') && obj.includes('pass')) {
    headers = {
      'user-agent': 'Custom-Request EBCOM',
      Authorization: `Basic ${Buffer.from(`${auth.user}:${auth.pass}`).toString('base64')}`,
    };
  } else if (obj.includes('bearer')) {
    headers = {
      'user-agent': 'Custom-Request EBCOM',
      Authorization: `Bearer ${auth.bearer}`,
    };
  } else throw new Error(`Authorization felids is not valid \n ${JSON.stringify(auth)}`);

  return headers;
};

// Set proxy with agent
const setAgent = (proxy) => ({
  agent: {
    http: new HttpProxyAgent({
      keepAlive: true,
      keepAliveMsecs: 1000,
      maxSockets: 256,
      maxFreeSockets: 256,
      scheduling: 'lifo',
      proxy,
    }),
  },
});

module.exports = async (params, http2, method) => {
  let response;

  const options = {
    url: params.url,
    method,
    allowGetBody: !!params.body,
    searchParams: params.query || undefined,
    headers: params.headers || {},
    agent: {},
    https: params.https || undefined,
    json: params.body || undefined,
    form: params.form || undefined,
    timeout: params.timeout || config.defaults.timeout || 60 * 1000,
    retry: params.retry || undefined,
    hooks: params.hooks || undefined,
    encoding: params.encoding || undefined,
    responseType: params.json === true ? 'json' : 'text',
    resolveBodyOnly: ([true, false].includes(params.resolveBodyOnly)) ? params.resolveBodyOnly : true,
    context: params.context || undefined,
    http2,
  };

  if (params.auth) Object.assign(options.headers, authorization(params.auth));
  if (params.proxy) Object.assign(options.agent, setAgent(params.proxy || config.defaults.proxy));

  try {
    response = await got(options);
  } catch (e) {
    if (['ETIMEDOUT', 'ECONNRESET', 'ESOCKETTIMEDOUT'].includes(e.code)) Object.assign(e, { error: { code: e.code } });
    throw e;
  }

  return response;
};
